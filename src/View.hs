{-# LANGUAGE OverloadedStrings #-}

module View where

import Control.Monad (when)
import qualified Clay as C
import Data.Monoid ((<>))
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Lucid

import Model

mainCss :: C.Css
mainCss = do
  C.body C.? 
    C.backgroundColor  C.beige
  C.pre C.? do
    C.backgroundColor  C.oldlace
    C.display          C.inlineBlock
    C.padding          (C.px 8) (C.px 8) (C.px 8) (C.px 8) 
    C.border           C.solid (C.px 1) C.black

myRender :: Html () -> T.Text -> L.Text
myRender body title = renderText $ do
  doctype_
  html_ $ do
    header_ $ do
        style_ $ L.toStrict $ C.render mainCss
        meta_ [charset_ "utf-8"]
        title_ $ toHtml $ "Autoquizer - " <> title
        link_ [rel_ "shortcut icon", href_ "/static/favicon.ico"]
    body_ $ do
      h1_ $ toHtml title
      body

errPage :: L.Text -> T.Text -> L.Text
errPage msg = myRender (p_ $ toHtml msg)

homePage :: T.Text -> L.Text
homePage = myRender $ do
  h2_ "Log in"
  form_ [action_ "/start", method_ "post"] $ do
      p_ "Lastname:"
      p_ $ input_ [name_ "lastname"]
      p_ "Firstname:"
      p_ $ input_ [name_ "firstname"]
      p_ "Password:"
      p_ $ input_ [name_ "password", type_ "password"]
      p_ $ input_ [type_ "submit", value_ "Submit"]

startPage :: Student -> Int -> Int -> T.Text -> L.Text
startPage student nbQuestions qzTime = myRender $ do
  h2_ "Instructions"
  ul_ $ do
    let minutesStr = L.pack $ show $ div qzTime 60
        questionsStr = L.pack $ show nbQuestions
    li_ $ toHtml $ L.concat [minutesStr, " minutes"]
    li_ $ toHtml $ L.concat [questionsStr, " questions"]
  form_ [action_ "/quiz", method_ "post"] $ do
      input_ [type_ "hidden", name_ "ids", value_ (T.pack $ show $ idS student)]
      input_ [type_ "submit", value_ "Start"]

quizPage :: Student -> Question -> [Choice] -> Int -> Int -> T.Text -> L.Text
quizPage student question choices dt dq = myRender $ do
  formatStudent student dt dq
  formatQuestion question
  form_ [action_ "/quiz", method_ "post"] $ do
    mapM_ formatChoice choices
    p_ ""
    input_ [type_ "hidden", name_ "ids", value_ (T.pack $ show $ idS student)]
    input_ [type_ "hidden", name_ "idq", value_ (T.pack $ show $ idQ question)]
    input_ [type_ "submit", value_ "Submit"]

formatStudent :: Student -> Int -> Int -> Html ()
formatStudent (Student _ l f _ _) dt dq = do
  div_ [style_ "float: left"] $ do
    toHtml $ L.concat ["Lastname: ", l]
    br_ []
    toHtml $ L.concat ["Firstname: ", f]
  div_ [style_ "float: left; margin-left:100px"] $ do
    toHtml $ L.concat ["Remaining questions: ", L.pack $ show dq]
    br_ []
    toHtml $ L.concat ["Remaining time: ", L.pack $ show dt, " s"]
  div_ [style_ "clear: both"] ""

formatQuestion :: Question -> Html ()
formatQuestion question = do
  h2_ "Question"
  let code = codeQ question
      image = imageQ question
  when (image /= "") (p_ $ img_ [src_ (L.toStrict image)])
  when (code /= "") (pre_ $ toHtmlRaw code)
  p_ $ toHtml $ textQ question

formatChoice :: Choice -> Html ()
formatChoice (Choice idc _ txt)  = do
  input_ [type_ "checkbox", name_ (T.pack $ show idc)] 
  toHtml $ L.toStrict txt
  br_ []

