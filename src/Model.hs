{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text.Lazy as L
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

openDb :: String -> IO SQL.Connection
openDb = SQL.open

closeDb :: SQL.Connection -> IO ()
closeDb = SQL.close

withTransaction :: SQL.Connection -> IO a -> IO a
withTransaction = SQL.withTransaction 

-- Student

data Student = Student
  { idS :: Int
  , lastnameS :: L.Text
  , firstnameS :: L.Text
  , passwordS :: L.Text
  , epochS :: Int
  } deriving (Show)

instance FromRow Student where
  fromRow = Student <$> field <*> field <*> field <*> field <*> field

addStudent :: SQL.Connection -> L.Text -> L.Text -> L.Text -> IO ()
addStudent conn l f p = do
  let req = "INSERT INTO student(lastnameS, firstnameS, passwordS, epochS) VALUES (?, ?, ?, ?)"
  SQL.execute conn req (l, f, p, 0::Int)

getOrCreateStudent :: SQL.Connection -> L.Text -> L.Text -> L.Text -> IO (Maybe Student)
getOrCreateStudent conn l f p = do
  studentM <- getStudentFromInfo conn l f
  case studentM of
    Just student -> return $ if passwordS student == p then studentM else Nothing
    Nothing -> do
      addStudent conn l f p
      getStudentFromInfo conn l f

getStudentFromInfo :: SQL.Connection -> L.Text -> L.Text -> IO (Maybe Student)
getStudentFromInfo conn l f = do
  let req = "SELECT * FROM student WHERE lastnameS=? AND firstnameS=?"
  res <- SQL.query conn req (l, f) :: IO [Student]
  return $ if null res then Nothing else Just (head res)

getStudentFromId :: SQL.Connection -> Int -> IO Student
getStudentFromId conn i = do
  let req = "SELECT * FROM student WHERE idS=?"
  res <- SQL.query conn req (SQL.Only i) :: IO [Student]
  return $ head res

getAndUpdateStudent :: SQL.Connection -> Int -> Int -> IO Student
getAndUpdateStudent conn ids t = do
  let req = "SELECT * FROM student WHERE idS=?"
  res <- SQL.query conn req (SQL.Only ids) :: IO [Student]
  let s@(Student i l f p e) = head res
  if e /= 0
    then return s
    else do
      SQL.execute conn "UPDATE student SET epochS=? WHERE idS=?" (t, i)
      return (Student i l f p t)

getStudents :: SQL.Connection -> IO [Student]
getStudents conn = SQL.query_ conn "SELECT * FROM student ORDER BY lower(lastnameS)" 

-- Question

data Question = Question
  { idQ :: Int
  , imageQ :: L.Text
  , codeQ :: L.Text
  , textQ :: L.Text
  } deriving (Show)

instance FromRow Question where
  fromRow = Question <$> field <*> field <*> field <*> field

getOneRandomQuestion :: SQL.Connection -> Student -> IO (Maybe Question)
getOneRandomQuestion conn student = do
  let req = "SELECT * FROM question WHERE idQ NOT IN (SELECT DISTINCT questionR FROM response WHERE studentR=?) ORDER BY RANDOM() LIMIT 1"
  res <- SQL.query conn req (SQL.Only $ idS student) :: IO [Question]
  return $ if null res then Nothing else Just $ head res

getNbQuestions :: SQL.Connection -> IO Int
getNbQuestions conn = do
  let req = "SELECT COUNT(*) FROM question"
  res <- SQL.query_ conn req :: IO [[Int]]
  return $ head $ head res

-- Choice

data Choice = Choice
  { idC :: Int
  , questionC :: Int
  , textC :: L.Text
  } deriving (Show)

instance FromRow Choice where
  fromRow = Choice <$> field <*> field <*> field

getChoicesOfQuestionM :: SQL.Connection -> Maybe Question -> IO [Choice]
getChoicesOfQuestionM _ Nothing = return []
getChoicesOfQuestionM conn (Just question) = do
  let req = "SELECT * FROM choice WHERE questionC=? ORDER BY RANDOM()"
  SQL.query conn req (SQL.Only $ idQ question) :: IO [Choice]

-- Response

data Response = Response
  { studentR :: Int
  , questionR :: Int
  , textR :: L.Text
  } deriving (Show)

instance FromRow Response where
  fromRow = Response <$> field <*> field <*> field

addResponse :: SQL.Connection -> Int -> Int -> L.Text -> IO ()
addResponse conn ids idq rsTxt = do
  let req = "INSERT OR IGNORE INTO response VALUES (?, ?, ?)"
  SQL.execute conn req (ids, idq, rsTxt)

getNbResponsesStudent :: SQL.Connection -> Student -> IO Int
getNbResponsesStudent conn student = do
  let req = "SELECT count(questionR) FROM response WHERE studentR=?"
  res <- SQL.query conn req (SQL.Only $ idS student) :: IO [[Int]]
  return $ head $ head res

getResponsesOfStudent :: SQL.Connection -> Int -> IO [Response]
getResponsesOfStudent conn ids = do
  let req = "SELECT * FROM response WHERE studentR=?"
  SQL.query conn req (SQL.Only ids) :: IO [Response]

getResponseOfStudent :: SQL.Connection -> Int -> Int -> IO [Response]
getResponseOfStudent conn i q = do
  let req = "SELECT * FROM response WHERE studentR=? AND questionR=?"
  SQL.query conn req (i, q) :: IO [Response]

