{-# LANGUAGE OverloadedStrings #-}

module Config where

import Control.Monad.Trans (liftIO)
import Data.Ini.Config (section, string, number, fieldOf, IniParser, parseIniFile)
import Data.Text (Text, pack)
import System.Environment (getArgs, getProgName)

data Config = Config
  { cfDatabase :: DatabaseConfig
  , cfServer :: ServerConfig
  , cfQuiz :: QuizConfig
  , cfScore :: ScoreConfig
  } deriving (Eq, Show)

data DatabaseConfig = DatabaseConfig
  { dbFilename :: String
  , dbTeacherId :: Int
  } deriving (Eq, Show)

newtype ServerConfig = ServerConfig
  { svPort :: Int
  } deriving (Eq, Show)

data QuizConfig = QuizConfig
  { qzTitle :: Text
  , qzTime :: Int
  } deriving (Eq, Show)

data ScoreConfig = ScoreConfig
  { scRight :: Double
  , scWrong :: Double
  , scNil :: Double
  } deriving (Eq, Show)

configParser :: IniParser Config
configParser = do
  dbCf <- section "DATABASE" $ do
    filename <- fieldOf "filename" string
    teacherId <- fieldOf "teacher-id" number
    return $ DatabaseConfig filename teacherId
  svCf <- section "SERVER" $ 
    ServerConfig <$> fieldOf "port" number
  qzCf <- section "QUIZ" $ do
    title <- fieldOf "title" string
    time <- fieldOf "time" number
    return $ QuizConfig title time
  scCf <- section "SCORE" $ do
    right <- fieldOf "k-right" number
    wrong <- fieldOf "k-wrong" number
    nil <- fieldOf "k-nil" number
    return $ ScoreConfig right wrong nil
  return $ Config dbCf svCf qzCf scCf

runAppConfig :: (Config -> IO ()) -> IO ()
runAppConfig app = do
  args <- getArgs
  progName <- getProgName
  if length args /= 1
    then putStrLn $ "usage: " ++ progName ++ " <config file>"
    else do
      configContent <- liftIO $ readFile $ head args
      let configE = parseIniFile (pack configContent) configParser
      runApp configE
  where runApp (Left err) = putStrLn err
        runApp (Right config) = app config

