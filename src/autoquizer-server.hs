{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (when)
import Control.Monad.Trans (liftIO)
import Data.List (sort)
import qualified Data.Text.Lazy as L
import Data.Time.Clock.POSIX (getPOSIXTime)
import Web.Scotty (middleware, scotty, get, html, params, param, 
                   post, file, redirect, rescue)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)

import Config
import Model
import View

remainingTime :: Student -> Int -> Int -> Int
remainingTime s currentT quizT = quizT - currentT + epochS s

main :: IO ()
main = runAppConfig serverApp

serverApp :: Config -> IO ()
serverApp config = do
  let dbname = dbFilename $ cfDatabase config
      title = qzTitle $ cfQuiz config
      time = qzTime $ cfQuiz config
  conn <- openDb dbname

  scotty (svPort $ cfServer config) $ do
    middleware logStdoutDev

    get "/" $ html $ homePage title

    get "/errpass" $ html $ errPage "Invalid password." title

    get "/errquestion" $ html $ errPage "Finished. No more question." title

    get "/errtime" $ html $ errPage "Finished. No more time." title

    post "/start" $ do
      firstname <- param "firstname"
      lastname <- param "lastname"
      password <- param "password"
      (studentM, nbQuestions) <- liftIO $ withTransaction conn $ do
        s <- getOrCreateStudent conn lastname firstname password
        n <- getNbQuestions conn
        return (s, n)
      case studentM of
        Nothing -> redirect "/errpass"
        Just student -> html $ startPage student nbQuestions time title

    post "/quiz" $ do
      t <- liftIO $ round <$> getPOSIXTime
      ids <- param "ids"
      idq <- param "idq" `rescue` (\_ -> return 0)
      ps <- params
      let rs = foldl (\acc (c,v) -> if v /= "on" then acc else c:acc) [] ps 
          rsTxt = L.unwords $ sort rs
      (student, questionM, choices, nbQuestions, nbResponses) <- liftIO $ withTransaction conn $ do
        when (idq /= 0) (addResponse conn ids idq rsTxt)
        s <- getAndUpdateStudent conn ids t
        q <- getOneRandomQuestion conn s
        c <- getChoicesOfQuestionM conn q
        nq <- getNbQuestions conn
        nr <- getNbResponsesStudent conn s
        return (s, q, c, nq, nr)

      case questionM of
        Nothing -> redirect "/errquestion"
        Just question -> do
          let dt = remainingTime student t time
              dq = nbQuestions - nbResponses
          if dt < 0
            then redirect "/errtime"
            else html $ quizPage student question choices dt dq title

    get "/static/:myfile" $ do
      myfile <- param "myfile"
      file $ "./static/" ++ myfile

  closeDb conn

