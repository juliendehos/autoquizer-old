{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy as L
import qualified Database.SQLite.Simple as SQL
import Text.Printf (printf)

import Config
import Model

main :: IO ()
main = runAppConfig scoreApp

scoreApp :: Config -> IO ()
scoreApp config = do
  conn <- openDb dbname
  students <- getStudents conn
  teacherResponses <- getResponsesOfStudent conn teacherId
  results <- mapM (computeScoreStudent conn teacherResponses ks) students
  closeDb conn
  putStrLn "lastname; firstname; score; nb right; nb wrong; nb nil"
  mapM_ print results
  where dbname = dbFilename $ cfDatabase config
        teacherId = dbTeacherId $ cfDatabase config
        kRight = scRight $ cfScore config
        kWrong = scWrong $ cfScore config
        kNil = scNil $ cfScore config
        ks = [kRight, kWrong, kNil]

data Result = Result
  { lastnameR :: L.Text
  , firstnameR :: L.Text
  , gradeR :: Double
  , nbRightR :: Int
  , nbWrongR :: Int
  , nbNilR :: Int
}

instance Show Result where
  show (Result l f g nr nw nn) = 
    printf "%s; %s; %.1f; %d; %d; %d" l f g nr nw nn

data Eval = EvalRight | EvalWrong | EvalNil deriving (Eq)

evalResponse :: SQL.Connection -> Student -> Response -> IO Eval
evalResponse conn s tRep = do
  sReps <- getResponseOfStudent conn (idS s) (questionR tRep)
  if null sReps 
    then return EvalNil
    else do
      let tTxt = textR tRep
          sTxt = textR $ head sReps
      if tTxt == sTxt 
        then return EvalRight
        else return $ if sTxt == "" then EvalNil else EvalWrong

computeScoreStudent :: SQL.Connection -> [Response] -> [Double] -> Student -> IO Result
computeScoreStudent conn teacherResponses ks stud@(Student _ l f _ _) = do
  results <- mapM (evalResponse conn stud) teacherResponses
  let fResults e = length $ filter (==e) results
      evalValues = [EvalRight, EvalWrong, EvalNil]
      ns@[nr, nw, nn] = map fResults evalValues
      nsd = map fromIntegral ns
      n = fromIntegral $ length teacherResponses
      s = sum $ zipWith (*) nsd ks
      score = s / n
  return $ Result l f score nr nw nn

