

# autoquizer

[![License MIT](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)
[![Build status](https://gitlab.com/juliendehos/autoquizer/badges/master/build.svg)](https://gitlab.com/juliendehos/autoquizer/pipelines) 

A simple web app for running quizzes + automatic scoring.


* [Overview](#overview)
* [Features](#features)
* [Install with Stack (recommended for most users)](#install-with-stack-recommended-for-most-users)
* [Install with Nix](#install-with-nix)
* [Try the example](#try-the-example)
* [Make your own quiz](#make-your-own-quiz)



## Overview

1. launch the server:

```
$ autoquizer-server quiz_1.cfg

Setting phasers to stun... (port 3000) (ctrl-c to quit)

GET /
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Status: 200 OK 0.000027599s

...
```

2. students log in and do the quiz:

![](doc/ss_login.png) ![](doc/ss_quiz.png)


3. compute scores:

```
$ autoquizer-score quiz_1.cfg 

lastname; firstname; score; nb right; nb wrong; nb nil
dehos; julien; -1.0; 0; 3; 0
Doe; John; 20.0; 3; 0; 0
Roblochon; Joel; 13.0; 2; 1; 0
```

4. export these results as CSV data:

![](doc/ss_csv.png)

5. there's no 5.


## Features

- The teacher runs `autoquizer-server` on a computer and the students do the quiz in a browser.

- The teacher can then get results and compute the student scores automatically (using `autoquizer-score`).

- Questions and choices are ordered randomly.

- Questions can include images and text samples.

- Lightweight setup: `autoquizer-server` is a standalone program. No web server required. No database server required. Works on the Internet or on a local network.

- Implemented with Haskell and SQLite.


## Install with Stack (recommended for most users)

- install [Stack](https://docs.haskellstack.org) then:

```
stack setup
stack build
stack install
```


## Install with Nix 

- install [Nix](https://nixos.org/nix) then:

```
nix-env -f . -i
```


## Try the example

- reset database:

```
cd quiz_1
rm -f quiz_1.db 
sqlite3 quiz_1.db < quiz_1.sql
```

- run server:

```
autoquizer-server quiz_1.cfg
```

The quiz can be done at `http://example.org:3000` (change `example.org` by your server name, your server ip or `localhost`).


- compute scores: 

```
autoquizer-score quiz_1.cfg
```


## Make your own quiz

See `quiz_1` as an example.

- create a new directory:

```
mkdir quiz_2
cd quiz_2
```

- add a configuration file, e.g. `quiz_2.cfg`:

```ini
[DATABASE]
# name or the database file
filename = quiz_2.db

# id of the teacher in the database
# (to define the correct answers)
teacher-id = 1

[SERVER]
port = 3000

[QUIZ]
title = Quiz 2
time = 60          # in seconds

[SCORE]
k-right = 20       # coefficient for a right answer
k-wrong = -1       # coefficient for a wrong answer
k-nil = 0          # coefficient for a missing answer
```

- make the quiz in a SQL file, e.g. `quiz_2.sql`:

```sql
-- Create the required tables (just copy that).

CREATE TABLE student (
  idS INTEGER,
  lastnameS TEXT,
  firstnameS TEXT,
  passwordS TEXT,
  epochS INTEGER,
  PRIMARY KEY (idS)
  UNIQUE (lastnameS, firstnameS)
);

CREATE TABLE question (
  idQ INTEGER,
  imageQ TEXT,
  codeQ TEXT,
  textQ TEXT,
  PRIMARY KEY (idQ)
);

CREATE TABLE choice (
  idC INTEGER,
  questionC INTEGER,
  textC TEXT,
  PRIMARY KEY (idC, questionC),
  FOREIGN KEY (questionC) REFERENCES question(idQ)
);

CREATE TABLE response (
  studentR INTEGER,
  questionR INTEGER,
  textR TEXT,
  PRIMARY KEY (studentR, questionR)
);

-- Insert a student which will define the correct answer.
-- ID should match "teacher-id" in quiz_2.cfg (here "1").
INSERT INTO student VALUES(1, 'Doe', 'John', '', 0);

-- Finally insert the questions...

-- ... question (here, "2" is the ID of the question)
INSERT INTO question VALUES (2, 
  'static/haskell-logo.svg', 
  'toto :: Int -> Int<br>toto x = r<br>  where r = 2*x', 
  'The result of "toto 21" is ?');

-- ... possible choices (use the ID of the question, here "2")
INSERT INTO choice VALUES (1, 2, 'an Int');
INSERT INTO choice VALUES (2, 2, 'a Double');
INSERT INTO choice VALUES (3, 2, '42');
INSERT INTO choice VALUES (4, 2, 'Luke, I''m your father !');
INSERT INTO choice VALUES (5, 2, 'undefined is not a function');
INSERT INTO choice VALUES (6, 2, 'segfault');

-- ... correct answer (teacher ID, question ID and answer)
-- The answer is a string containing the choice IDs
-- (space-separated, IN ASCENDING ORDER).
INSERT INTO response VALUES (1, 2, '1 3');

...
```


- setup database:

```
sqlite3 quiz_2.db < quiz_2.sql
```


