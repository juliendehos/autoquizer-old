-- sqlite3 quiz_1.db < quiz_1.sql

CREATE TABLE student (
  idS INTEGER,
  lastnameS TEXT,
  firstnameS TEXT,
  passwordS TEXT,
  epochS INTEGER,
  PRIMARY KEY (idS)
  UNIQUE (lastnameS, firstnameS)
);

CREATE TABLE question (
  idQ INTEGER,
  imageQ TEXT,
  codeQ TEXT,
  textQ TEXT,
  PRIMARY KEY (idQ)
);

CREATE TABLE choice (
  idC INTEGER,
  questionC INTEGER,
  textC TEXT,
  PRIMARY KEY (idC, questionC),
  FOREIGN KEY (questionC) REFERENCES question(idQ)
);

CREATE TABLE response (
  studentR INTEGER,
  questionR INTEGER,
  textR TEXT,
  PRIMARY KEY (studentR, questionR)
);

INSERT INTO student VALUES(1, 'Doe', 'John', '', 0);

INSERT INTO question VALUES (2, 
  'static/haskell-logo.svg', 
  'toto :: Int -> Int<br>toto x = r<br>  where r = 2*x', 
  'The result of "toto 21" is ?');
INSERT INTO choice VALUES (1, 2, 'an Int');
INSERT INTO choice VALUES (2, 2, 'a Double');
INSERT INTO choice VALUES (3, 2, '42');
INSERT INTO choice VALUES (4, 2, 'Luke, I''m your father !');
INSERT INTO choice VALUES (5, 2, 'undefined is not a function');
INSERT INTO choice VALUES (6, 2, 'segfault');
INSERT INTO response VALUES (1, 2, '1 3');

INSERT INTO question VALUES (1, 
  '', 
  'int f(int x) {<br>  return x*2;<br>}', 
  'True equals ?');
INSERT INTO choice VALUES (1, 1, 'True');
INSERT INTO choice VALUES (2, 1, 'False');
INSERT INTO choice VALUES (3, 1, '42');
INSERT INTO response VALUES (1, 1, '1');

INSERT INTO question VALUES (3, 
  'static/haskell-logo.svg', 
  '', 
  'Haskell is ?');
INSERT INTO choice VALUES (1, 3, 'a functional programming language');
INSERT INTO choice VALUES (2, 3, 'a lazy-evaluated language');
INSERT INTO choice VALUES (3, 3, 'a prototype-based object-oriented language');
INSERT INTO response VALUES (1, 3, '1 2');

